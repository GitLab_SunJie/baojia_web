module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? './' : '/', // 本地HTML使用./相对路径
    lintOnSave: false,
    productionSourceMap: false,
    css: {
        loaderOptions: {
            // 给 stylus-loader 传递选项
            stylus: {
                import: '~@/assets/css/common.styl'
            }
        }
    },
    devServer: {
        open: false,
        host: '0.0.0.0',
        port: 8888,
        https: false,
        hotOnly: false,
        proxy: {
            '/api': {
                target: 'http://localhost:8080',
                //target: 'http://localhost:8080',
                changeOrigin: true,
                pathRewrite: {
                    '^/api': '/'
                }
            }
            // '/upload': {//获取图片资源
            //     target: 'http://localhost:8080',// 正式服：
            //     ws:true,
            //     changeOrigin: true,
            //     pathRewrite:{
            //         '^/upload':'/'
            //     }
            // },
        }
    },
}
