import axios from 'axios';
import util from '@/utils/utils';
import store from '@/store';
import router from '../router/index';
import {Message,Loading } from 'element-ui';
const newAxios = axios.create();

if (process.env.NODE_ENV == 'development') { //开发环境
	newAxios.defaults.baseURL = 'api';
} else if (process.env.NODE_ENV == 'production') {//生产环境
	newAxios.defaults.baseURL = 'api';
}
newAxios.defaults.timeout = 60*1000;// 请求超时时间1分钟
newAxios.interceptors.request.use(config => {//添加请求拦截器
	config.headers = {
		'BROWSER_TYPE':'PC',
		'BRAND':window.navigator.userAgent,
		'VERSION':'1.0.1',
		'Content-Type':'application/json;charset=UTF-8',
		'Access-token': util.isBlank(sessionStorage.getItem('token'))?localStorage.getItem('token'):sessionStorage.getItem('token'),
	}
	return config;
}, (error) => {
	return Promise.reject(error);
});
//响应拦截器
newAxios.interceptors.response.use( 
	response=> {
		if(response.data.code == 200){
			return Promise.resolve(response);
		}else{
			switch(response.data.code){
				case 1102:
					setTimeout(function() {
						router.replace({
							path: 'login',
							query: { backUrl: router.currentRoute.fullPath },
						})
					}, 1500);
					break;
				case 1300:
					setTimeout(function() {
						router.replace({
							path: '403',
							query: { backUrl: router.currentRoute.fullPath },
						})
					}, 1500);
					break;	
				case 4010:
					setTimeout(function() {
						router.replace({
							path: 'login',
							query: { backUrl: router.currentRoute.fullPath },
						})
					}, 1500);
					break;
				default:
					response.data.code = -100
					response.data.message = '访问错误！'
					break;	
			}
			return Promise.reject(response.data);
		}
	}, 
	err =>{
		let result = {code:0, message:''};
		if(err.response){
			switch (err.response.status) {
				case 400:
					result.code = err.response.status
					result.message = err.response.statusText
					break;
				case 401:
					result.code = err.response.status
					result.message = err.response.statusText
					// 只有在当前路由不是登录页面才跳转
					setTimeout(function(){
						router.replace({
							path: '/login',
							query: { backUrl: router.currentRoute.fullPath },
						})
					},1500)
					break;
				case 403:
					result.code = err.response.status
					result.message = err.response.statusText
					break;
				case 404:
					result.code = err.response.status
					result.message = err.response.statusText
					setTimeout(function(){
						router.push({
							path: '/404', 
							query: {} 
						});
					},1500)
					break;
				case 408:
					result.code = err.response.status
					result.message = err.response.statusText
					break;
							
				case 500:
					result.code = err.response.status
					result.message = '网络请求错误'
					break;
							
				case 501:
					result.code = err.response.status
					result.message = err.response.statusText
					break;
							
				case 502:
					result.code = err.response.status
					result.message = err.response.statusText
					break;
							
				case 503:
					result.code = err.response.status
					result.message = err.response.statusText
					break;
							
				case 504:
					result.code = err.response.status
					result.message = err.response.statusText
					break;
							
				case 505:
					result.code = err.response.status
					result.message = err.response.statusText
					break;
				default:
					result.code = -100
					result.message = '访问错误！'
					break;	
			}
		}else{
			result.code = 9999;
			result.message = '未知错误'
		}
		return Promise.reject(result);
	}
)
// 请求方法处理
export function httpService (url, method, params, showLoading) {
	if(util.isBlank(showLoading)||showLoading){
		var loading = Loading.service({
		    lock: true,
		    text: '加载中...',
		    spinner: 'el-icon-loading',
		    background: 'rgba(0, 0, 0, 0.7)'
		});
	}
	return new Promise((resolve, reject) => {
		newAxios({
			url: url,
			method: !util.isBlank(method)?method:'POST',
			data: method === 'post' ? !util.isBlank(params) ? params :'' : null,
			params: method === 'get' ? !util.isBlank(params) ? params :'' : null
		}).then( (res) =>{
			if(util.isBlank(showLoading)||showLoading){loading.close()}
			resolve(res.data);
		}).catch(error => {
			if(util.isBlank(showLoading)||showLoading){loading.close()}
			resolve(error);
		})
	})
}









