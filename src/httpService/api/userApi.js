import { httpService } from '../http';

/* ------------------------用户---------------------- */
const catcha = params => httpService('/user/catcha','post',params,false);// 获取获取登录图片验证码
const login = params => httpService('/user/login', 'post', params);//登录
const list = params => httpService('/user/list', 'post', params);//获取用户列表
const add = params => httpService('/user/add', 'post', params);//添加用户
const del = params => httpService('/user/delete', 'post', params);//删除
const edit = params => httpService('/user/update', 'post', params);//修改
const info = params => httpService('/user/info', 'post', params);//获取用户信息

/* ------------------------ 图片 ---------------------- */
const uploadPic = params => httpService('', 'post', params);//图片上传

export default{
    catcha,
    login, 
    list,
    add,
    info,
    del,
    edit,
    uploadPic,
}