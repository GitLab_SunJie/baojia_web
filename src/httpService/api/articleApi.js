import { httpService } from '../http';

const items = params => httpService('/item/items', 'post', params, false);//获取栏目列表
const list = params => httpService('/article/list', 'post', params);//获取列表
const add = params => httpService('/article/add', 'post', params);//添加
const del = params => httpService('/article/delete', 'post', params);//删除
const edit = params => httpService('/article/update', 'post', params);//修改
const info = params => httpService('/article/info', 'post', params);//获取详情

export default{
    items,
    list,
    add,
    del,
    edit,
    info
}