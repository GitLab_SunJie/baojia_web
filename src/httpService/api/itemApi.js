import { httpService } from '../http';

const list = params => httpService('/item/list', 'post', params);//获取列表
const add = params => httpService('/item/add', 'post', params);//添加
const del = params => httpService('/item/delete', 'post', params);//删除
const edit = params => httpService('/item/update', 'post', params);//修改
const info = params => httpService('/item/info', 'post', params);//获取详情

export default{
    list,
    add,
    del,
    edit,
    info
}