import { fileService } from './fileHttp';
import { httpService } from './http';

const del = params => httpService('/file/del', 'post', params);//删除图片
const uploadPic = params => fileService('/file/upload', 'post', params)//上传图片

export default{
    del,
    uploadPic
}