export default {
    menuList: [
        {
            name: "首页",
            value: "1",
        },
        {
            name: "关于宝佳",
            value: "2",
        },
        {
            name: "学历直通车",
            value: "3",
        },
        {
            name: "专业设置",
            value: "4",
        },
        {
            name: "就业服务",
            value: "5",
        },
        {
            name: "校区师资",
            value: "6",
        },
        {
            name: "国家助学",
            value: "7",
        },
        {
            name: "新闻动态",
            value: "8",
        },
        {
            name: "精彩视频",
            value: "9",
        },
        {
            name: "在线报名",
            value: "10",
        },
    ],
    subMenuList: [
        { 
            name: "大数据工程师", 
            value: "1" ,
            childList:[
                {name:'撒打发斯蒂芬',value:'1_1'},
                {name:'水电费我发的',value:'1_2'},
                {name:'烦得很大使馆的个人苟富贵',value:'1_3'},
                {name:'胜多负少',value:'1_4'},
            ]
        },
        { 
            name: "视觉设计系列", 
            value: "2",
            childList:[
                {name:'威尔士多',value:'2_1'},
                {name:'十字相乘v',value:'2_2'},
                {name:'会尽快与',value:'2_3'},
                {name:'全额热无若群翁',value:'2_4'},
            ]
        },
        { 
            name: "空间设计系列", 
            value: "3",
            childList:[
                {name:'其维持秩序',value:'3_1'},
                {name:'规划局目标',value:'3_2'},
                {name:'只需单位',value:'3_3'},
                {name:'儿童问问',value:'3_4'},
            ]
        },
        { 
            name: "电商设计系列", 
            value: "4" ,
            childList:[
                {name:'想办法好人',value:'4_1'},
                {name:'法规和认同和',value:'4_2'},
                {name:'阿尔上档次',value:'4_3'},
                {name:'用途特人文',value:'4_4'},
            ]
        },
        { 
            name: "运维工程师", 
            value: "5" ,
            childList:[
                {name:'主程序稍等',value:'5_1'},
                {name:'会尽快回家',value:'5_2'},
                {name:'娃儿3',value:'5_3'},
                {name:'我认为二无',value:'5_4'},
            ]
        },
        { 
            name: "IT培训学员", 
            value: "6", 
            childList:[
                {name:'生栋覆屋',value:'6_1'},
                {name:'筛选出',value:'6_2'},
                {name:'体育会',value:'6_3'},
                {name:'去问问二无群',value:'6_4'},
            ]
        },
    ],
    studentList: [
        {
            name: '李雪淋',
            photo: '年薪15万',
            money: require('../images/index/李雪淋.jpg'),
            work: '运维工程师'
        },
        {
            name: '任鑫',
            photo: '年薪20万',
            money: require('../images/index/任鑫.jpg'),
            work: '室内设计师'
        },
        {
            name: '袁覃成',
            photo: '年薪30万',
            money: require('../images/index/袁覃成.jpg'),
            work: '大数据工程师'
        },
        {
            name: '刘艺拧',
            photo: '年薪18万',
            money: require('../images/index/刘艺柠.jpg'),
            work: 'UI设计师'
        },
        {
            name: '苏鹏',
            photo: '年薪15万',
            money: require('../images/index/苏鹏.jpg'),
            work: '平面设计师'
        },
        {
            name: '李智慧',
            photo: '年薪22万',
            money: require('../images/index/李智慧.jpg'),
            work: 'UI设计师'
        },
        {
            name: '万海鹏',
            photo: '年薪30万',
            money: require('../images/index/万海鹏.jpg'),
            work: '大数据工程师'
        },
        {
            name: '申东',
            photo: '年薪30万',
            money: require('../images/index/申东.jpg'),
            work: '室内设计师'
        },
    ],
    doubtList: [
        {
            img: require('../images/index/18.png'),
            title: '零基础怎么学UI设计',
        },
        {
            img: require('../images/index/18.png'),
            title: '这家机构靠谱吗',
        },
        {
            img: require('../images/index/18.png'),
            title: '是否能保障就业',
        },
        {
            img: require('../images/index/18.png'),
            title: '学不会怎么办',
        },
        {
            img: require('../images/index/18.png'),
            title: '有适合我学习的课程么',
        },
        {
            img: require('../images/index/18.png'),
            title: 'IT就业前景怎么样',
        },
        {
            img: require('../images/index/18.png'),
            title: '没有基础可以学编程么',
        },
        {
            img: require('../images/index/18.png'),
            title: '软件工程师毕业一般工资是多少',
        }
    ],
    ticherList: [
        {
            name:'杨老师',
            photo: require('../images/index/杨.png'),
            workYear: '10年设计经验',
            teachYear:'',
            project:'',
            work: '室内设计老师'
        },
        {
            name: '吴老师',
            photo: require('../images/index/吴.png'),
            workYear: '8年软件开发经验',
            teachYear:'',
            project:'参与和主导过浦发银行、渤海银行、中国移动、贵州银行等核心业务系统开发，具有丰富的J2EE、SSM、分布式框架、大数据技术等系统项目开发经验',
            work: 'JAVA讲师'
        },
        {
            name: '张老师',
            photo: require('../images/index/张.png'),
            workYear: '4年设计经验',
            teachYear:'5年教学经验',
            project:'腾讯，中国移动，美团',
            work: 'UI课程讲师'
        },
        {
            name: '翁老师',
            photo: require('../images/index/翁.png'),
            workYear: '15年软件开发经验',
            teachYear:'',
            project:'',
            work: '大数据讲师'
        },
        {
            name: '秦老师',
            photo: require('../images/index/秦.png'),
            workYear: '7年设计经验',
            teachYear:'',
            project:'',
            work: '室内设计老师'
        },
        {
            name: '马老师',
            photo: require('../images/index/马.png'),
            workYear: '3年设计经验',
            teachYear:'',
            project:'中国联通，饿了么，蚂蚁金服',
            work: 'UI课程讲师'
        },
        {
            name: '文老师',
            photo: require('../images/index/文.png'),
            workYear: '4年设计经验',
            teachYear:'',
            project:'团队协作设计开发LIFE，趣票APP等具有丰富的项目策划、海报设计、人像精修、摄影、UI项目等实战经验',
            work: 'UI课程讲师'
        },
        {
            name: '胡老师',
            photo: require('../images/index/胡.png'),
            workYear: '5年设计经验',
            teachYear:'',
            project:'与和主导过发布会海报及物料设计、奶牛梦工厂微商界面 设计;团队协作设计开发LIFE、趣票APP等具有丰富的项目策划、海报设计、人像精修、摄影、UI项目等实战经验',
            work: 'UI课程讲师'
        },
        {
            name: '帅老师',
            photo: require('../images/index/帅.png'),
            workYear: '20年软件开发经验',
            teachYear:'',
            project:'联通3G网管系统建设、贵州移动双中心建设、贵州移动云计算中心贵安资源池系统等大型项目的开发并担任技术总监',
            work: '运维工程讲师'
        },
        {
            name: '王老师',
            photo: require('../images/index/王.png'),
            workYear: '8年设计经验',
            teachYear:'',
            project:'',
            work: '室内设计老师'
        },
    ],
    designerList:[
        {
            name:'文老师',
            photo: require('../images/components/about/15.png'),
            workYear: '4年设计工作经验',
            teachYear:'',
            project:'曾在网易、广州、南橙等多家企业任职，任职期间担任公司UI设计师，获得设计学院设计奖和多彩贵州设计优秀奖',
            work: 'UI讲师'
        },
        {
            name:'文老师',
            photo: require('../images/components/about/15.png'),
            workYear: '4年设计工作经验',
            teachYear:'',
            project:'曾在网易、广州、南橙等多家企业任职，任职期间担任公司UI设计师，获得设计学院设计奖和多彩贵州设计优秀奖',
            work: 'UI讲师'
        },
        {
            name:'文老师',
            photo: require('../images/components/about/15.png'),
            workYear: '4年设计工作经验',
            teachYear:'',
            project:'曾在网易、广州、南橙等多家企业任职，任职期间担任公司UI设计师，获得设计学院设计奖和多彩贵州设计优秀奖',
            work: 'UI讲师'
        },
        {
            name:'文老师',
            photo: require('../images/components/about/15.png'),
            workYear: '4年设计工作经验',
            teachYear:'',
            project:'曾在网易、广州、南橙等多家企业任职，任职期间担任公司UI设计师，获得设计学院设计奖和多彩贵州设计优秀奖',
            work: 'UI讲师'
        },
        {
            name:'文老师',
            photo: require('../images/components/about/15.png'),
            workYear: '4年设计工作经验',
            teachYear:'',
            project:'曾在网易、广州、南橙等多家企业任职，任职期间担任公司UI设计师，获得设计学院设计奖和多彩贵州设计优秀奖',
            work: 'UI讲师'
        },
        {
            name:'文老师',
            photo: require('../images/components/about/15.png'),
            workYear: '4年设计工作经验',
            teachYear:'',
            project:'曾在网易、广州、南橙等多家企业任职，任职期间担任公司UI设计师，获得设计学院设计奖和多彩贵州设计优秀奖',
            work: 'UI讲师'
        },
        {
            name:'文老师',
            photo: require('../images/components/about/15.png'),
            workYear: '4年设计工作经验',
            teachYear:'',
            project:'曾在网易、广州、南橙等多家企业任职，任职期间担任公司UI设计师，获得设计学院设计奖和多彩贵州设计优秀奖',
            work: 'UI讲师'
        },
        {
            name:'文老师',
            photo: require('../images/components/about/15.png'),
            workYear: '4年设计工作经验',
            teachYear:'',
            project:'曾在网易、广州、南橙等多家企业任职，任职期间担任公司UI设计师，获得设计学院设计奖和多彩贵州设计优秀奖',
            work: 'UI讲师'
        },
    ]
}
