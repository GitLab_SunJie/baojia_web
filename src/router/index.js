import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home.vue'
import Index from '../pages/Index.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect:'/index' },
  { 
    path: '/',
    name: 'Home',
    component: Home,
    children:[
      {
        path: '/index',
        name:'index',
        component:Index,
        meta:{title:'宝佳睿云-首页', login_require:false}
      },
    ]
  },
  // { path: '/login', name:'login', component:Login, meta:{title:'登录'} },
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
